﻿using AGL.Pet.Common.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace AGL.Pet.Abstraction.Repositories
{
    public interface IPetRepository
    {
        Task<List<PetOwner>> FetchData();
    }
}
