﻿using AGL.Pet.Common.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace AGL.Pet.Abstraction.Services
{
    public interface IPetService
    {
        Task<Dictionary<string, List<PetInfo>>> FetchDataAsDictionary(PetType filter = PetType.Unspecified);
        
    }
}
