﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AGL.Pet.Abstraction.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace AGL.Pet.Api.Controllers
{
    public class HomeController : Controller
    {
        private readonly IPetService petService;
        private readonly ILogger<HomeController> logger;

        public HomeController(IPetService petService, ILogger<HomeController> logger)
        {
            this.petService = petService ?? throw new ArgumentNullException("petService");
            this.logger = logger ?? throw new ArgumentNullException("logger");
        }

        // GET: Home
        [Route("/")]
        [ApiExplorerSettings(IgnoreApi = true)]
        public async Task<ActionResult> Index()
        {
            try
            {
                var data = await this.petService.FetchDataAsDictionary(Common.Models.PetType.Cat);

                return View(data);
            }
            catch(Exception ex)
            {
                this.logger.LogError(ex.Message, ex);
                ModelState.AddModelError("message", "500 - Server Error occured, Could not load data.");
                return View(null);
            }
           
        }
        
    }
}