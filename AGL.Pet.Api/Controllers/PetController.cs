﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AGL.Pet.Abstraction.Services;
using AGL.Pet.Common.Exceptions;
using AGL.Pet.Common.Models;
using AGL.Pet.Common.Models.Responses;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;

namespace AGL.Pet.Api.Controllers
{
    [ApiVersion("1.0")]
    [ApiVersion("2.0")]
    [Route("api/v{version:apiVersion}/[controller]")]
    [Produces("application/json")]
    public class PetController : Controller
    {
        private readonly IPetService petService;
        private readonly IOptions<AppSetting> setting;
        private readonly ILogger<PetController> logger;

        public PetController(IPetService petService, IOptions<AppSetting> setting, ILogger<PetController> logger)
        {
            this.petService = petService;
            this.setting = setting;
            this.logger = logger;
            
        }

        // GET api/v1.0/values
        /// <summary>
        /// Get list of owner gender and their pets
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     GET /api/v1.0/pet
        ///
        /// </remarks>
        /// <returns>Return pet data</returns>
        /// <response code="200">Return list owner gender and pets in dictionary format</response>
        /// <response code="500">Server error</response>            
        [HttpGet, MapToApiVersion("1.0")]
        [ProducesResponseType(typeof(Dictionary<string, List<string>>), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(ErrorResponse), StatusCodes.Status500InternalServerError)]
        
        public async Task<IActionResult> GetV1()
        {
            try
            {
                var data = await this.petService.FetchDataAsDictionary(Common.Models.PetType.Cat);
                
                //transform data
                Dictionary<string, List<string>> result = new Dictionary<string, List<string>>();

                foreach (var item in data)
                {
                    result.Add(item.Key, item.Value.Select(x => x.Name).ToList());
                }

                return Json(result);
            }
            catch(PetBusinessException pex)
            {
                logger.LogError("PetBusinessException: " + pex.Message);
                return StatusCode(500, new ErrorResponse (){ ErrorMessage = "500 - Server Error occured, Could not load data. " });
            }
            catch (Exception ex)
            {
                logger.LogError("Unknow exception: " + ex.Message);
                return StatusCode(500, new ErrorResponse() { ErrorMessage= "500 - Unknow error. " });
            }
        }

        // GET api/v2.0/pet
        /// <summary>
        /// Get list of owner gender and their pets
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     GET /api/v2.0/pet
        ///
        /// </remarks>
        /// <returns>Return pet data</returns>
        /// <response code="200">Return list owner gender and pets list format</response>
        /// <response code="500">Server error</response>            
        [ProducesResponseType(typeof(PetResponse), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(ErrorResponse), StatusCodes.Status500InternalServerError)]
        [HttpGet, MapToApiVersion("2.0")]
        public async Task<IActionResult> GetV2()
        {
            try
            {
                var data = await this.petService.FetchDataAsDictionary(Common.Models.PetType.Cat);

                //transform data to array
                var transform = data.Select(x => new PetResponse()
                {
                    Gender = x.Key,
                    Pets = x.Value.Select(p => p.Name).ToList()
                }).ToList();

                return Json(transform);
            }
            catch (PetBusinessException ex)
            {
                logger.LogError("PetBusinessException: " + ex.Message);
                return StatusCode(500, new ErrorResponse() { ErrorMessage = "500 - Server Error occured, Could not load data. " });
            }
            catch (Exception ex)
            {
                logger.LogError("Unknow exception: " + ex.Message);
                return StatusCode(500, new ErrorResponse(){ ErrorMessage = "500 - Unknow error. " });
            }
        }
    }
}
