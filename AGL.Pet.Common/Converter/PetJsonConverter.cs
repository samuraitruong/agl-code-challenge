﻿using AGL.Pet.Common.Models;
using AGL.Pet.Common.Extensions;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;

namespace AGL.Pet.Common.Converter
{
    public class PetJsonConverter : JsonConverter
    {
        public override bool CanConvert(Type t) => t == typeof(PetType) || t == typeof(PetType?);

        public override object ReadJson(JsonReader reader, Type t, object existingValue, JsonSerializer serializer)
        {
            if (t == typeof(PetType))
                return PetTypeExtensions.ReadJson(reader, serializer);
            if (t == typeof(PetType?))
            {
                if (reader.TokenType == JsonToken.Null) return null;
                return PetTypeExtensions.ReadJson(reader, serializer);
            }
            throw new Exception("Unknown type");
        }

        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            var t = value.GetType();
            if (t == typeof(PetType))
            {
                ((PetType)value).WriteJson(writer, serializer);
                return;
            }
            throw new Exception("Unknown type");
        }

        public static readonly JsonSerializerSettings Settings = new JsonSerializerSettings
        {
            MetadataPropertyHandling = MetadataPropertyHandling.Ignore,
            DateParseHandling = DateParseHandling.None,
            Converters = {
                new PetJsonConverter(),
                new IsoDateTimeConverter { DateTimeStyles = DateTimeStyles.AssumeUniversal }
            },
        };
    }
}
