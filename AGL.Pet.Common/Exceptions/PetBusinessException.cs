﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AGL.Pet.Common.Exceptions
{
    public class PetBusinessException : Exception
    {
        public PetBusinessException(string message) : base(message)
        {
        }
    }
}
