﻿using AGL.Pet.Common.Models;
using Newtonsoft.Json;
using System;

namespace AGL.Pet.Common.Extensions
{
    static class PetTypeExtensions
    {
        public static PetType? ValueForString(string str)
        {
            switch (str)
            {
                case "Cat": return PetType.Cat;
                case "Dog": return PetType.Dog;
                case "Fish": return PetType.Fish;
                default: return PetType.Unknow;
            }
        }

        public static PetType ReadJson(JsonReader reader, JsonSerializer serializer)
        {
            var str = serializer.Deserialize<string>(reader);
            var maybeValue = ValueForString(str);
            if (maybeValue.HasValue) return maybeValue.Value;
            throw new Exception("Unknown enum case " + str);
        }

        public static void WriteJson(this PetType value, JsonWriter writer, JsonSerializer serializer)
        {
            switch (value)
            {
                case PetType.Cat: serializer.Serialize(writer, "Cat"); break;
                case PetType.Dog: serializer.Serialize(writer, "Dog"); break;
                case PetType.Fish: serializer.Serialize(writer, "Fish"); break;
                case PetType.Unknow: serializer.Serialize(writer, "Unknow"); break;
            }
        }
    }
}
