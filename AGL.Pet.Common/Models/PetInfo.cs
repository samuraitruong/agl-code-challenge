﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace AGL.Pet.Common.Models
{
    public class PetInfo
    {
        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("type")]
        public PetType Type { get; set; }
    }
    public enum PetType { Cat, Dog, Fish, Unknow, Unspecified };

}
