﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;
using AGL.Pet.Common.Converter;
using AGL.Pet.Common.Models;

namespace AGL.Pet.Common.Models
{
    public partial class PetOwner
    {
        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("gender")]
        public string Gender { get; set; }

        [JsonProperty("age")]
        public long Age { get; set; }

        [JsonProperty("pets")]
        public List<PetInfo> Pets { get; set; }
    }
    
    public partial class PetOwner
    {
        public static List<PetOwner> FromJson(string json) => JsonConvert.DeserializeObject<List<PetOwner>>(json, PetJsonConverter.Settings);
    }
       
}
