﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AGL.Pet.Common.Models.Responses
{
    public class ErrorResponse
    {
        public string ErrorMessage { get; set; }
        public int ErrorCode { get; set; }
    }
}
