﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AGL.Pet.Common.Models.Responses
{
    /// <summary>
    /// The Pet Response model
    /// </summary>
    public class PetResponse
    {
        public string Gender { get; set; }
        public List<string> Pets { get; set; }
    }
}
