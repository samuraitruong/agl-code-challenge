﻿using AGL.Pet.Common.Models;
using AGL.Pet.Abstraction.Repositories;
using AGL.Pet.Common.Exceptions;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;

namespace AGL.Pet.Repository
{
    public class PetRepository : IPetRepository
    {
        private readonly IOptions<AppSetting> options;

        public PetRepository(IOptions<AppSetting> options)
        {
            this.options = options ?? throw new ArgumentNullException("options", "an instance of AppSeting is required");
        }

        public async Task<List<PetOwner>> FetchData()
        {
            List<PetOwner> result = new List<PetOwner>();
            using (var client = new HttpClient())
            {
                var response = await client.GetAsync(options.Value.JsonDataSource);
                var json = await response.Content.ReadAsStringAsync();
                return PetOwner.FromJson(json);
            }
        }
    }
}
