﻿using AGL.Pet.Abstraction.Repositories;
using AGL.Pet.Abstraction.Services;
using AGL.Pet.Common.Exceptions;
using AGL.Pet.Common.Models;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AGL.Pet.Service
{
    public class PetService : IPetService
    {
        private readonly ILogger<PetService> logger;
        private readonly IPetRepository petRepository;

        public PetService(ILogger<PetService> logger, IPetRepository petRepository)
        {
            this.logger = logger ?? throw new ArgumentNullException("logger");
            this.petRepository = petRepository ?? throw new ArgumentNullException("petRepository"); ;
        }

        public async Task<Dictionary<string, List<PetInfo>>> FetchDataAsDictionary(PetType filter = PetType.Unspecified)
        {
           var result = new Dictionary<string, List<PetInfo>>();
            try
            {
                var data = await this.petRepository.FetchData();
                var grouped = data.Where(x => x.Pets != null && !string.IsNullOrEmpty(x.Gender)).GroupBy(x => x.Gender);
                foreach (var item in grouped)
                {
                    var pets = item.SelectMany(x => x.Pets.Where(p => !string.IsNullOrEmpty(p.Name) && (filter == PetType.Unspecified || p.Type == filter))).ToList();
                    result.Add(item.Key, pets.OrderBy(x => x.Name).ToList());
                }
                return result;
            }
            catch(AggregateException ex)
            {
                // Happen when network issue , enpoid not existing, json is invalid format
                logger.LogError(ex.Message + ex.StackTrace, ex);
                throw new PetBusinessException("Could not load Pet data");
            }
            catch(Exception ex)
            {
                //Unknow exception
                logger.LogError(ex.Message + ex.StackTrace, ex);
                throw ex;
            }
        }
    }
}
