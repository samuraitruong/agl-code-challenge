﻿using AGL.Pet.Common.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using Xunit;

namespace AGL.Pet.Tests
{
    public class JSonSerializationTests
    {
        List<PetOwner> Parse(string fileName) {
            string json = File.ReadAllText(fileName);
            return PetOwner.FromJson(json);
        }
        [Fact]
        [Trait("Category", "Json Serialization Test")]
        public void FromJson_ShouldWorks_When_ValidJson()
        {
            var obj = Parse("data\\FromJsonTest\\test1_validjson.json");
            Assert.NotNull(obj);
            Assert.Single(obj);
            var petOwner = obj.First();

            Assert.Single(petOwner.Pets);
            Assert.Equal("Jennifer", petOwner.Name);
            Assert.Equal("Female", petOwner.Gender);
            Assert.Equal(18, petOwner.Age);
            var pet = petOwner.Pets[0];

            Assert.Equal("Garfield", pet.Name);
            Assert.Equal(PetType.Cat, pet.Type);

        }

        [Theory]
        [Trait("Category", "Json Serialization Test")]
        [InlineData("data\\FromJsonTest\\test2_pettype_cat.json", PetType.Cat)]
        [InlineData("data\\FromJsonTest\\test3_pettype_fish.json", PetType.Fish)]
        [InlineData("data\\FromJsonTest\\test4_pettype_dog.json", PetType.Dog)]
        [InlineData("data\\FromJsonTest\\test5_pettype_other.json", PetType.Unknow)]
        [InlineData("data\\FromJsonTest\\test7_pettype_null.json", PetType.Unknow)]
        public void FromJson_Should_Deserialize_PetType_ToEnums(string dataFile, PetType expectedType)
        {
            var obj = Parse(dataFile);
            Assert.NotNull(obj);
            Assert.Single(obj);
            var petOwner = obj.First();

            Assert.Single(petOwner.Pets);
            Assert.Equal("Jennifer", petOwner.Name);
            Assert.Equal("Female", petOwner.Gender);
            Assert.Equal(18, petOwner.Age);
            var pet = petOwner.Pets[0];

            Assert.Equal("Garfield", pet.Name);
            Assert.Equal(expectedType, pet.Type);

        }

        [Fact]
        [Trait("Category", "Json Serialization Test")]
        public void FromJson_ShouldWorks_With_MultipleItems()
        {
            var data = Parse("data\\FromJsonTest\\test6_multiple_items.json");
            Assert.NotNull(data);
            Assert.Equal(2, data.Count);
            Assert.Single(data[0].Pets);
            Assert.Equal(2, data[1].Pets.Count);

            Assert.Equal("Jennifer", data[0].Name);
            Assert.Equal(32, data[0].Age);
            Assert.Equal("Female", data[0].Gender);

            Assert.Equal("Jennifer1", data[1].Name);
            Assert.Equal(16, data[1].Age);
            Assert.Equal("Male", data[1].Gender);

            Assert.Equal("Garfield2", data[1].Pets[1].Name);
            Assert.Equal(PetType.Cat, data[1].Pets[1].Type);

        }
    }
}
