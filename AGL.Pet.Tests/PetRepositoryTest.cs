using AGL.Pet.Abstraction.Repositories;
using AGL.Pet.Common.Exceptions;
using AGL.Pet.Common.Models;
using AGL.Pet.Repository;
using Microsoft.Extensions.Options;
using System;
using Xunit;

namespace AGL.Pet.Tests
{
    public class PetRepositoryTest
    {
        IOptions<AppSetting> settings;
        public PetRepositoryTest()
        {
            settings = Options.Create<AppSetting>(new AppSetting()
            {
                JsonDataSource = "http://agl-developer-test.azurewebsites.net/people.json"
            });
        }
        [Fact]
        public void FetchData_Should_FetchData_Successful()
        {
            IPetRepository repository = new PetRepository(settings);
            var data = repository.FetchData().Result;
            Assert.NotNull(data);
            Assert.NotEmpty(data);
        }

        [Fact]
        public void PetRepository_Should_Error_IfNoOptions()
        {
            Assert.Throws<ArgumentNullException>(()=>
            {
                IPetRepository repository = new PetRepository(null);
                var data = repository.FetchData().Result;
            });
        }

        [Fact]
        public void PetRepository_Should_ThrowException_When_InvalidJson()
        {
            var invalidOptions = Options.Create<AppSetting>(new AppSetting()
            {
                JsonDataSource = "https://google.com.vn"
            });

            Assert.Throws<AggregateException>(() =>
            {
                IPetRepository repository = new PetRepository(invalidOptions);
                var data = repository.FetchData().Result;
            });
        }

      

    }
}
