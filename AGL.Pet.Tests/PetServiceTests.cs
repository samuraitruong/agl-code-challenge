﻿using AGL.Pet.Abstraction.Repositories;
using AGL.Pet.Common.Models;
using AGL.Pet.Service;
using Microsoft.Extensions.Logging;
using NSubstitute;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace AGL.Pet.Tests
{
    public class PetServiceTests
    {
        private ILogger<PetService> logger;

        public PetServiceTests()
        {
            logger = Substitute.For<ILogger<PetService>>();
        }
        IPetRepository FakePetRepositoryWithJsonFile(string filename)
        {
            var data = ReadFromFile(filename);

            var petRepository = Substitute.For<IPetRepository>();
            petRepository.FetchData().Returns(data);
            return petRepository;
        }
        List<PetOwner> ReadFromFile(string file)
        {
            var json = File.ReadAllText(file);
            var data = PetOwner.FromJson(json);
            Assert.NotNull(data);
            return data;
        }
        [Fact]
        public void PetService_ReadAsDictionary_ShouldIgnore_NullPets()
        {

            var repository = FakePetRepositoryWithJsonFile("data\\testcase0_nullpets.json");

            Assert.Null(repository.FetchData().Result[0].Pets);

            var service = new PetService(logger, repository);
            var dict = service.FetchDataAsDictionary().Result;

            Assert.True(dict.Count == 1);
            Assert.False(dict.ContainsKey("Male"));
        }

        [Fact]
        public void PetService_ReadAsDictionary_Should_ExcludedNullOrEmpty_Gender()
        {
            var data = ReadFromFile("data\\testcase5_null_gender.json");

            var petRepository = Substitute.For<IPetRepository>();
            petRepository.FetchData().Returns(data);

            var service = new PetService(logger, petRepository);
            var dict = service.FetchDataAsDictionary().Result;
            
            Assert.True(dict.ContainsKey("Female"));
            Assert.Single(dict);
            Assert.Single(dict["Female"]);
            Assert.Equal("Garfield", dict["Female"][0].Name);

        }

        [Fact]
        public void PetService_ReadAsDictionary_Success()
        {
            var data = ReadFromFile("data\\testcase1_default.json");

            var petRepository = Substitute.For<IPetRepository>();
            petRepository.FetchData().Returns(data);

            var service = new PetService(logger, petRepository);
            var dict = service.FetchDataAsDictionary().Result;

            Assert.True(dict.ContainsKey("Male"));
            Assert.True(dict.ContainsKey("Female"));

        }
        [Theory]
        [InlineData(PetType.Cat)]
        [InlineData(PetType.Dog)]
        [InlineData(PetType.Fish)]
        public void PetService_ReadAsDictionary_By_Type(PetType type)
        {
            var repository = FakePetRepositoryWithJsonFile("data\\testcase2_by_type.json");

            var service = new PetService(logger, repository);
            var dict = service.FetchDataAsDictionary(type).Result;
            Assert.True(dict.Count == 1);
            if(type == PetType.Cat)
            {
                Assert.Equal(2, dict["Male"].Count);
            }
            foreach (var item in dict["Male"])
            {
                Assert.Equal(type, item.Type);
            }
            

        }
        [Fact]
        public void PetService_ReadAsDictionary_ShouldNotInclude_PetWithEmptyName()
        {
            var repository = FakePetRepositoryWithJsonFile("data\\testcase3_empty_name.json");

            var service = new PetService(logger, repository);
            var dict = service.FetchDataAsDictionary( PetType.Cat).Result;
            Assert.Equal(2, dict["Male"].Count);

            Assert.Equal("Fido", dict["Male"][0].Name);

            Assert.Equal("Garfield", dict["Male"][1].Name);


        }

        [Theory]
        [InlineData("data\\testcase4_order_alphabeta1.json", 4)]
        [InlineData("data\\testcase4_order_alphabeta2.json", 4)]
        public void PetService_ReadAsDictionary_ShouldSort_PetByAlphabetical(string datasource, int count)
        {
            var repository = FakePetRepositoryWithJsonFile(datasource);

            var service = new PetService(logger, repository);
            var dict = service.FetchDataAsDictionary(PetType.Cat).Result;
            Assert.Equal(count, dict["Male"].Count);
            var pets = dict["Male"];
            
            for(var i=1; i< pets.Count; i++)
            {
                Assert.True( String.CompareOrdinal( pets[i-1].Name , pets[i].Name) <0 );
            }

        }

        [Fact]
        [Trait("Category", "Pet Service Test")]
        public void PetService_ReadAsDictionary_Should_LogException()
        {
            var logger1 = Substitute.For<ILogger<PetService>>();
            var repository = Substitute.For<IPetRepository>();
            repository.FetchData().Returns(async (x) => { await Task.Run(() => { throw new Exception(""); }); } );

            logger1.When(x => x.LogError("dummy message")).Do(x => {  });

            var service = new PetService(logger1, repository);
            try
            {
                var dict = service.FetchDataAsDictionary(PetType.Cat).Result;
            }
            catch{       
            }
            logger1.Received().Log(
                 LogLevel.Error,
                 Arg.Any<EventId>(),
                 Arg.Is<object>(o => true),
                 null,
                 Arg.Any<Func<object, Exception, string>>());
        }

    }
}
