# AGL Code Challenge
The compiled version  of this application was deployed to azure at : [https://aglcodechallenge.azurewebsites.net](https://aglcodechallenge.azurewebsites.net)
## Objectives
Write an app/service to consume data from [http://agl-developer-test.azurewebsites.net/people.json](http://agl-developer-test.azurewebsites.net/people.json) and display as below format
Name is alphabetical sort order.
- Male
	- Angel
	- Molly
	- Tigger
- Female
	- Gizmo
	- Jasper

## Project structure

- AGL.Pet.Abstraction -> Defined prototype of interfaces for services and repositories
- AGL.Pet.Common -> Defined the common class such as utility, models, serialization, helper
- AGL.Pet.Repository -> Play as Data Access layer to do fetching raw data from IO device(Internet, Database, File...)
- AGL.Pet.Service -> Busines layer to process raw data, control business logic, caching, logging logic 
- AGL.Pet.API -> The actual application which plays roles of presentation layer. It responsible for getting request, validation data, transform data and response the final result to end user
- AGL.Pet.Tests -> The Unit test project to make sure all the implementation above works as expected. This project using xUnit which quite similar to NUnit or MSTest.

## Code Techniques
The project was designed using TDD and Repository pattern in respect of SOLID principle to make sure we have a clean, readable, easy to maintain and testable. This project using belows coding technologies
- .NET core 2.0
- .NET core Dependency Injection - Which is the key of testable code
- Configuration JSON base using Microsft.Extensions.configuration
- API Versioning
- Logging with Nlog and Microsoft.Extensions.Logging - NLog will provide multiple target and easy to plugin multiple log service into application just with configuration change (In real application we will add more target such as Sumologic, SQL, other cloud log provider)

### Dependency Injection
All dependency injection is configured at starting up time of the app inside starup.cs as the common structure of .NET core project

Inject services & repository class

```csharp
//IoC configuration
services.AddTransient<IPetRepository, PetRepository>();
services.AddTransient<IPetService, PetService>();
```
Inject the app setting

```csharp
services.Configure<AppSetting>(Configuration.GetSection("AppSetting"));
```
### Data Modeling

Base on the input JSON file, I use [https://app.quicktype.io](https://app.quicktype.io) to generate model for the app. (We can also modeling manually). this app also using Newtonsoft.Json library to serialize and deserialize object from JSON data.
- PetOwner
	- Name :String
	- Age : Interger
	- Gender: String("Male", "Female")
	- Pets: List of PetInfo
- PetInfo
	- Name : String
	- Type :String converted to enums PetType(Cat, Dog, Fish, Unknow)

This is raw data which app received from external system. After that we use dictionary to transform data to use in our app. benefit of using Dictionary is performance and memory optimize. It also close to our output format. 

### Logging configuration
The project using Microsoft.Extensions.Logging and Nlog Core to handle log message cross application. 

NLog integration
```csharp
 public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
{
	env.ConfigureNLog("nlog.config");
	//add NLog to ASP.NET Core
	loggerFactory.AddNLog();
	//add NLog.Web
	app.AddNLogWeb();
	...
}
```
This will tell system to using Nlog with all the configuration setup at nlog.config file. The the future if we want to add more target for stogare log message, we only need to update this file
Sample Nlog.config file
```xml
<?xml version="1.0"?>
<nlog xmlns="http://www.nlog-project.org/schemas/NLog.xsd" 
      xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
      autoReload="true"
      internalLogLevel="Warn"
      internalLogFile="logs\internal-nlog.txt">
  <extensions>
    <add assembly="NLog.Web.AspNetCore"/>
  </extensions>
  
  <targets>
    <target name="allfile" xsi:type="File" 
            fileName="${basedir}\logs\${shortdate}.log" 
            encoding="utf-8" 
            layout="[${longdate}][${machinename}][${level}] ${message} ${exception}" />
  </targets>
  <rules>
    <logger name="*" minlevel="Trace" writeTo="allfile" />
    <logger name="Microsoft.*" minlevel="Trace" writeTo="blackhole" final="true" />
  </rules>
</nlog>
```
Using Logger
After make the configuration, we can using logger any where in the system by inject ILogger interface
```csharp
 public HomeController(IPetService petService, ILogger<HomeController> logger)
{
	this.petService = petService;
	this.logger = logger;
}
// Then we can use 
this.logger.LogError(ex.Message, ex);
```

### Application configuration 
In this simple app, we only have 1 endpoint URL need to be configured, We use Microsoft.Extensions.Configuration with IOptions supports auto reload to read the configuration from appsettings.json file. this library support the configuration can be load from multiple sources such as environment variables, azure property, JSON file, XML file.
appsettings.json 

```json
{
  "Logging": {
    "IncludeScopes": false,
    "Debug": {
      "LogLevel": {
        "Default": "Trace"
      }
    },
    "Console": {
      "LogLevel": {
        "Default": "Trace"
      }
    }
  },
  "AppSetting": {
    "JsonDataSource": "http://agl-developer-test.azurewebsites.net/people.json"
  }
}

```
appsettings.json is environment specific. In production environemtn the setting will be load from appsetting.production.json which base on ASPNETCORE_ENVIRONMENT in the environment setup.

Below code is used to inject setting in starup.cs

```csharp
services.Configure<AppSetting>(Configuration.GetSection("AppSetting"));
// To use this setting we will use inject via IOption<AppSetting> 
private readonly IOptions<AppSetting> options;

public PetRepository(IOptions<AppSetting> options)
{
	this.options = options ?? throw new ArgumentNullException("options", "an instance of AppSeting is required");
}
// Then we can access the configuration value 
await client.GetAsync(this.options.Value.JsonDataSource);
```

### PetRepository
Play as data access layer was injected via DI thought IPetRepository. In this app we using HttpClient to fetch raw json string from end-point and convert to Model using Newtonsoft.Json . In the complex application we may consider use RestSharp as a client for using it advance features

```csharp
 public async Task<List<PetOwner>> FetchData()
{
	List<PetOwner> result = new List<PetOwner>();
	using (var client = new HttpClient()) //Should re-use instance of HttpClient instead
	{
		var response = await client.GetAsync(options.Value.JsonDataSource);
		var json = await response.Content.ReadAsStringAsync();
		return PetOwner.FromJson(json); //Extension method using Newtonsoft.Json deserialize
	}
}
```

### PetService
Play business layer role to process data after received it from remote endpoint by calling PetRepository.FetchData. Below are logics apply

- Filter only owner has gender (ignore owner with gender null or empty)
- Filter only owner has Pets 
- PetType fill passed into function to filter by PetType. If not pass(IE PetType.Unspecified) this filter will be ignored
- The list of Pet then sort by alphabetical order
- Don’t remove duplicated pet name (not in requirement)

Note: We keep all PetInfo in data return from service layer, Presentation layer will responsible for transform data to the final expected format
Code
```csharp
public async Task<Dictionary<string, List<PetInfo>>> FetchDataAsDictionary(PetType filter = PetType.Unspecified)
{
	var result = new Dictionary<string, List<PetInfo>>();
	try
	{
		var data = await this.petRepository.FetchData();
		var grouped = data.Where(x => x.Pets != null && !string.IsNullOrEmpty(x.Gender)).GroupBy(x => x.Gender);
		foreach (var item in grouped)
		{
			var pets = item.SelectMany(x => x.Pets.Where(p => !string.IsNullOrEmpty(p.Name) && (filter == PetType.Unspecified || p.Type == filter))).ToList();
			result.Add(item.Key, pets.OrderBy(x => x.Name).ToList());
		}
		return result;
	}
	catch(AggregateException ex)
	{
		// Happen when network issue , enpoid not existing, json is invalid format
		logger.LogError(ex.Message + ex.StackTrace, ex);
		throw new PetBusinessException("Could not load Pet data");
	}
	catch(Exception ex)
	{
		//Unknow exception
		logger.LogError(ex.Message + ex.StackTrace, ex);
		throw ex;
	}
}
```
### Presentation 
Presentation in our application are PetController and HomeController which actually does the same thing but in different format
#### HomeController
Call PetService to get a dictionary data and using Razor view to generate the list of Gender and Pets name

#### PetController
This is the Web API to return data to client in JSON format. It does data transformation to the client expected format.

Output format V1 /api/v1.0/pet
```json
{"Male":["Garfield","Jim","Max","Tom"],"Female":["Garfield","Simba","Tabby"]}
```
I prefer this format instead of array because it is more optimized and very easy to use at client side, however, if client doesn't like this format we can easy update the transform function to get below format

API V2 /api/v2.0/pet
```json
[{"gender":"Male","pets":["Garfield","Jim","Max","Tom"]},{"gender":"Female","pets":["Garfield","Simba","Tabby"]}]
```

### Exception handling
We use Exception & custom Exception to handle error between different layer. In our application, we don't have any logic or data to rollback so when exception happen system only need to log it using NLog target. In general, Exception will be handled as below

- Repository will not handle any error unless there is transaction support rollback data.
- Service will catch all known exception and handle it such as handle some business logic, rollback committed data, logging, notification..... after that Service layer will re-throw (same, or new Exception) to presentation layer
- Presentation layer (Controller) will catch the custom exception and response to client with friendly message such as HttpCode (2xx, 4xx, 5xx)

### API Versioning
As we maintain the API and keep develop system as it grows. API may be out of date but we already have clients integrate to using our API so that we needs add versioning into API to keep develop system without changing existing current version. In this project  API versioning is implemented using ASP https://www.nuget.org/packages/Microsoft.AspNetCore.Mvc.Versioning

Different API maybe just a format of input/output message. Sometime, it can be different database, logic process...

Pet service API
V1 http://aglcodechallenge.azurewebsites.net/api/v1.0/pet

V2 http://aglcodechallenge.azurewebsites.net/api/v2.0/pet

Versioning configuration in startup.cs
```csharp
services.AddApiVersioning(o =>
{
	o.AssumeDefaultVersionWhenUnspecified = true;
	o.DefaultApiVersion = new ApiVersion(1, 0);
} ;
```
## Unit Test

The unit test project is xUnit which is the best unit test for .NET core as my opinion. However, I have been using NUnit and MSTest so it doesn't matter which framework we use. 

Unit test helps us to test the code and make sure we passed all the logic and validation. It also helps us save time to do regression test when we do code change on existing function/feature. Unit test can also use as a part of automation test during release/CI process so that unit test become a required in any code, any language today.

In this project I use NSubstitute to Mock object
Example of NSubstitute
```csharp
var data = ReadFromFile("data\\testcase5_null_gender.json");

var petRepository = Substitute.For<IPetRepository>();
petRepository.FetchData().Returns(data);
```
Alternative, we can use Moq as it the biggest library for .NET Mock test.
## API Documentation
The project was configure to use [Swashbuckle.AspNetCore](https://github.com/domaindrivendev/Swashbuckle.AspNetCore) to generate restful API document. The API document can be view using swagger UI at [http://aglcodechallenge.azurewebsites.net/swagger/](http://aglcodechallenge.azurewebsites.net/swagger/). The document will be extracted using XML documentation in source file

## Authentication

No authentication required to access API(We can use OAuth or JWT implement authentication for for web API)

##  Build & Release
This project is base on .NET project template so we can use below method to build it
- using dotnet command like
```bash
cs [solution directory]
dotnet build AGL.Pet.Api.sln

To release 
dotnet publish -out [directory output]

run test
dotnet test [assembly test]
or dotnet test dotnet test AGL.Pet.Tests\AGL.Pet.Tests.csproj
```

- The easy way is using Visual Studio 2017 or Visual Studio code (with extensions installed for .NET cor project)

## Limitations
- Unit test not include Controller test
